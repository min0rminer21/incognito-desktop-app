import { ipcMain as ipc } from 'electron-better-ipc';
import { INCOGNITO_STORAGE_EVENTS } from './events';
import { setStorage, getStorage, deleteStorage } from './method';

export const listen = () => {
  ipc.answerRenderer(
    INCOGNITO_STORAGE_EVENTS.SET,
    ({ key, value }: { key: string; value: unknown }) => setStorage(key, value)
  );

  ipc.answerRenderer(INCOGNITO_STORAGE_EVENTS.GET, (key: string) =>
    getStorage(key)
  );

  ipc.answerRenderer(INCOGNITO_STORAGE_EVENTS.DELETE, (key: string) =>
    deleteStorage(key)
  );
};
