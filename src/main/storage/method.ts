import Store from 'electron-store';

const store = new Store();

export const setStorage = (key: string, value: any) => {
  return store.set(key, value);
};

export const getStorage = (key: string) => {
  return store.get(key);
};

export const deleteStorage = (key: string) => {
  return store.delete(key);
};
