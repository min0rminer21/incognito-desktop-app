const parseName = (name) => `INCO_STORAGE_${name}`;

const INCOGNITO_STORAGE_EVENTS = {
  SET: parseName('SET'),
  GET: parseName('GET'),
  DELETE: parseName('DELETE'),
};

module.exports = {
  INCOGNITO_STORAGE_EVENTS,
};
