import { ipcMain as ipc } from 'electron-better-ipc';
import BigNumber from 'bignumber.js';
import { INCOGNITO_CLI_EVENTS } from './events';
import {
  createNewAccount,
  getKeyInfo,
  submitOtaKey,
  getBalance,
  send,
} from './method';

export const listen = () => {
  ipc.answerRenderer(INCOGNITO_CLI_EVENTS.CREATE_NEW_ACCOUNT, () =>
    createNewAccount()
  );

  ipc.answerRenderer(INCOGNITO_CLI_EVENTS.GET_KEY_INFO, (privateKey: string) =>
    getKeyInfo(privateKey)
  );

  ipc.answerRenderer(INCOGNITO_CLI_EVENTS.SUBMIT_OTA_KEY, (otaKey: string) =>
    submitOtaKey(otaKey)
  );

  ipc.answerRenderer(
    INCOGNITO_CLI_EVENTS.GET_BALANCE,
    (data: { privateKey: string; tokenID?: string }) => {
      const { privateKey, tokenID } = data;
      return getBalance(privateKey, tokenID);
    }
  );

  ipc.answerRenderer(
    INCOGNITO_CLI_EVENTS.SEND_TX,
    (data: {
      privateKey: string;
      tokenID?: string;
      address: string;
      amountStr: string;
    }) => {
      const { privateKey, tokenID, address, amountStr } = data;
      const amount = new BigNumber(amountStr);
      return send(privateKey, address, amount, tokenID);
    }
  );
};
