import BigNumber from 'bignumber.js';
import { getAssetPath } from '../../util';
import { execCmd } from '../exec';
import { IncognitoCliType } from './types';

const FULLNODE_HOST = 'https://beta-fullnode.incognito.org/fullnode';

const pathCli = getAssetPath('incognito-cli', 'cli');
const cmdCli = `${pathCli} --host ${FULLNODE_HOST}`;

/**
 * THIS SERVICE'S USING STDOUT TO HANDLE RESULT OF COMMANDS
 * THIS IS NOT REALLY GOOD SOLUTION, BUT ENOUGH FOR NOW
 * I WILL FIND ANOTHER BETTER SOLUTION TO PROCESS THIS OR IF YOU HAVE ANY IDEAS
 * AND WANT TO SHARE IT, THANK YOU!
 */

/**
 * Generate a new mnemonic phrase and its Incognito account.
 * https://github.com/incognitochain/incognito-cli#generateaccount
 */
export const createNewAccount =
  async (): Promise<IncognitoCliType.CreateAccountRes> => {
    const rawRes = await execCmd(`${cmdCli} generateaccount`);
    const memonicStr = rawRes.split('\n')[0];
    const memonic = memonicStr.replace('mnemonic: ', '');
    const keys = JSON.parse(rawRes.replace(memonicStr, ''));

    return { memonic, keys };
  };

/**
 * Get all related-keys of a private key.
 * https://github.com/incognitochain/incognito-cli#keyinfo
 */
export const getKeyInfo = async (
  privateKey: string
): Promise<IncognitoCliType.AccountKeys> => {
  const rawRes = await execCmd(`${cmdCli} keyinfo --privateKey ${privateKey}`);
  const keys = JSON.parse(rawRes);

  return keys;
};

/**
 * Submit an ota key to the full-node.
 * https://github.com/incognitochain/incognito-cli#submitkey
 */
export const submitOtaKey = async (otaKey: string): Promise<void> => {
  await execCmd(`${cmdCli} submitkey --otaKey ${otaKey}`);
};

/**
 * Check the balance of an account.
 * https://github.com/incognitochain/incognito-cli#balance
 * @returns balance (BigNumber)
 */
export const getBalance = async (
  privateKey: string,
  tokenID?: string
): Promise<string> => {
  const rawRes = await execCmd(
    `${cmdCli} balance --privateKey ${privateKey}${
      tokenID ? ` --tokenID ${tokenID}` : ''
    }`
  );

  return rawRes;
};

/**
 * Send an amount of PRV or token from one wallet to another wallet.
 * https://github.com/incognitochain/incognito-cli/tree/main#send
 * @returns tx id (string)
 */
export const send = async (
  privateKey: string,
  address: string,
  amount: BigNumber,
  tokenID?: string
): Promise<string> => {
  const amountStr = amount.toString();
  const rawRes = await execCmd(
    `${cmdCli} send --privateKey ${privateKey} --address ${address} --amount ${amountStr}${
      tokenID ? ` --tokenID ${tokenID}` : ''
    }`
  );

  const resultStr = rawRes.split('\n')?.[2];
  const detectStr = 'Success!! TxHash ';
  const isSuccess = resultStr.includes(detectStr);

  if (isSuccess) {
    return resultStr.replace(detectStr, '');
  }
  throw new Error('Send transaction failed');
};
