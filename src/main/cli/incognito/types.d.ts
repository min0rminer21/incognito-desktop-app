export namespace IncognitoCliType {
  export interface AccountKeys {
    MiningKey: string;
    MiningPublicKey: string;
    OTAPrivateKey: string;
    PaymentAddress: string;
    PrivateKey: string;
    PublicKey: string;
    ReadOnlyKey: string;
  }

  export interface CreateAccountRes {
    memonic: string;
    keys: AccountKeys;
  }
}
