const parseName = (name) => `INCO_CLI_${name}`;

const INCOGNITO_CLI_EVENTS = {
  CREATE_NEW_ACCOUNT: parseName('CREATE_NEW_ACCOUNT'),
  GET_KEY_INFO: parseName('GET_KEY_INFO'),
  SUBMIT_OTA_KEY: parseName('SUBMIT_OTA_KEY'),
  GET_BALANCE: parseName('GET_BALANCE'),
  SEND_TX: parseName('SEND_TX'),
};

module.exports = {
  INCOGNITO_CLI_EVENTS,
};
