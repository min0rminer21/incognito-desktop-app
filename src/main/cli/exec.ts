import { exec, ExecException } from 'child_process';

const execLog = (
  cmd: string,
  error: ExecException | null,
  stdout?: string,
  stderr?: string
) => {
  let contains = `\nEXECUTE: ${cmd}`;
  if (error) {
    contains += `\n >>> ERROR: ${error.message}`;
  }
  if (stdout) {
    contains += `\n   >>> STDOUT: ${stdout}`;
  }
  if (stderr) {
    contains += `\n   >>> STDERR: ${stderr}`;
  }

  console.log(contains);

  // TODO write to log file
};

export const execCmd = (cmd: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      execLog(cmd, error, stdout, stderr);
      if (error) {
        return reject(error.message);
      }
      if (stderr) {
        return reject(stderr);
      }
      return resolve(stdout);
    });
  });
};
