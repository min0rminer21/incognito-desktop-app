const { contextBridge } = require('electron');
const BigNumber = require('bignumber.js');
const { ipcRenderer } = require('electron-better-ipc');
const { INCOGNITO_CLI_EVENTS } = require('./cli/incognito/events');
const { INCOGNITO_STORAGE_EVENTS } = require('./storage/events');

contextBridge.exposeInMainWorld('electron', {
  storage: {
    set(key, value) {
      ipcRenderer.callMain(INCOGNITO_STORAGE_EVENTS.SET, { key, value });
    },
    get(key) {
      return ipcRenderer.callMain(INCOGNITO_STORAGE_EVENTS.GET, key);
    },
  },
  incognitoCli: {
    createNewAccount() {
      return ipcRenderer.callMain(INCOGNITO_CLI_EVENTS.CREATE_NEW_ACCOUNT);
    },
    getKeyInfo(privateKey) {
      if (typeof privateKey === 'string') {
        return ipcRenderer.callMain(
          INCOGNITO_CLI_EVENTS.GET_KEY_INFO,
          privateKey
        );
      }
      throw new Error('getKeyInfo requires a private key!');
    },
    submitOtaKey(otaKey) {
      if (typeof otaKey === 'string') {
        return ipcRenderer.callMain(
          INCOGNITO_CLI_EVENTS.SUBMIT_OTA_KEY,
          otaKey
        );
      }
      throw new Error('submitOtaKey requires a OTA key!');
    },
    getBalance({ privateKey, tokenID }) {
      if (typeof privateKey === 'string') {
        return ipcRenderer.callMain(INCOGNITO_CLI_EVENTS.GET_BALANCE, {
          privateKey,
          tokenID,
        });
      }
      throw new Error('getBalance requires a privateKey!');
    },
    send({ privateKey, tokenID, address, amountStr }) {
      if (typeof privateKey !== 'string') {
        throw new Error('send requires a privateKey!');
      }

      if (typeof address !== 'string') {
        throw new Error('send requires a address!');
      }

      if (amountStr instanceof BigNumber) {
        throw new Error('send requires a valid amount!');
      }

      return ipcRenderer.callMain(INCOGNITO_CLI_EVENTS.SEND_TX, {
        privateKey,
        tokenID,
        address,
        amountStr,
      });
    },
  },
});
