// DEFINE BASE ON src/main/preload.js

export interface IncognitoStorageMethods {
  get: <T>(key: string) => Promise<T | undefined>;
  set: (key: string, value: any) => void;
  delete: (key: string) => Promise<void>;
}
