import type { IncognitoCliType } from '../../main/cli/incognito/types';

// DEFINE BASE ON src/main/preload.js

export interface IncognitoCliMethods {
  createNewAccount: () => Promise<IncognitoCliType.CreateAccountRes>;

  getKeyInfo: (privateKey: string) => Promise<IncognitoCliType.AccountKeys>;

  submitOtaKey: (otaKey: string) => Promise<void>;

  getBalance: (data: {
    privateKey: string;
    tokenID?: string;
  }) => Promise<string>;

  send: (data: {
    privateKey: string;
    address: string;
    amountStr: string;
    tokenID?: string;
  }) => Promise<string>;
}
