import useToken from '../../../hooks/useToken';
import TokenDetail from './TokenDetail';

function ListFavToken() {
  const { getFavTokens } = useToken();
  const favTokens = getFavTokens();

  return (
    <div>
      {favTokens.map((token) => (
        <TokenDetail
          key={token.tokenId}
          token={{
            name: token.name,
            symbol: token.symbol,
            networkName: token.networkName,
            tokenId: token.tokenId,
          }}
        />
      ))}
    </div>
  );
}

export default ListFavToken;
