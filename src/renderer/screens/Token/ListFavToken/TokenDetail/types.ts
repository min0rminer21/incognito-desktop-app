export interface IToken {
  name: string;
  symbol: string;
  tokenId: string;
  networkName: string;
}

export interface IProps {
  token: IToken;
}
