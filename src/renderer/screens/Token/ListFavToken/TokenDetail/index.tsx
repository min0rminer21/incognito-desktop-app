import { Card, Row } from 'antd';
import TokenBalance from '../../../../components/TokenBalance';
import Token from '../../../../components/Token';
import { IProps } from './types';
import styles from './styles.scss';
import FavoriteTokenToggle from '../../../../components/FavoriteTokenToggle';
import SendTransaction from '../../../../components/SendTransaction';

function TokenDetail(props: IProps) {
  const { token } = props;

  return (
    <Card className={styles.container}>
      <Row justify="space-between">
        <Token
          token={{
            name: token.name,
            symbol: token.symbol,
            networkName: token.networkName,
            tokenId: token.tokenId,
          }}
        />
        <FavoriteTokenToggle tokenId={token.tokenId} />
      </Row>
      <TokenBalance tokenId={token.tokenId} />
      <SendTransaction tokenId={token.tokenId} />
    </Card>
  );
}

export default TokenDetail;
