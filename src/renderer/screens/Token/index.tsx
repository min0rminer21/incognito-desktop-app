import ListAllToken from './ListAllToken';
import ListFavToken from './ListFavToken';

function Token() {
  return (
    <div>
      <ListAllToken />
      <ListFavToken />
    </div>
  );
}

export default Token;
