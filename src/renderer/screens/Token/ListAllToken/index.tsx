import { List, Input, Modal, Button } from 'antd';
import { sortBy } from 'lodash-es';
import { useCallback, useState } from 'react';
import useToken from '../../../hooks/useToken';
import styles from './styles.scss';
import Token from '../../../components/Token';
import FavoriteTokenToggle from '../../../components/FavoriteTokenToggle';

function ListAllToken() {
  const { getAllTokenList, getFavTokens } = useToken();
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [isShowModal, setIsShowModal] = useState(false);
  const tokenList = getAllTokenList();
  const favToken = getFavTokens();

  const getTokenListToDisplay = useCallback(() => {
    const filteredList = tokenList.filter((token) =>
      `${token.name} ${token.symbol}`
        .toLowerCase()
        .includes(searchTerm.toLowerCase())
    );

    const sorted = sortBy(
      filteredList,
      (item) => favToken.find((token) => token.tokenId === item.tokenId)?.name
    );

    return sorted;
  }, [searchTerm, tokenList, favToken]);

  const listTokenListToDisplay = getTokenListToDisplay();

  return (
    <div className={styles.container}>
      <Button type="primary" onClick={() => setIsShowModal(true)}>
        Add more token
      </Button>
      <Modal
        title="Add token"
        visible={isShowModal}
        footer={null}
        onCancel={() => setIsShowModal(false)}
      >
        <div>
          <Input
            placeholder="Search for token name"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
            style={{ marginBottom: 16 }}
          />

          <List
            itemLayout="horizontal"
            dataSource={listTokenListToDisplay}
            pagination={{
              showSizeChanger: false,
              pageSize: 6,
            }}
            renderItem={(item) => (
              <List.Item
                extra={
                  <FavoriteTokenToggle
                    tokenId={item.tokenId}
                    onAdded={() => setSearchTerm('')}
                  />
                }
              >
                <Token
                  token={{
                    name: item.name,
                    symbol: item.symbol,
                    networkName: item.networkName,
                    tokenId: item.tokenId,
                  }}
                />
              </List.Item>
            )}
          />
        </div>
      </Modal>
    </div>
  );
}

export default ListAllToken;
