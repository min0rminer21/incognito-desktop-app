import { useEffect } from 'react';
import { Redirect } from 'react-router';
import { ROUTE_PATH } from '../../routers/path';
import { loadPtokenSetAction } from '../../state/app/action';
import { useAppDispatch } from '../../state/store';

function Splash() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(loadPtokenSetAction());
  }, [dispatch]);

  return <Redirect to={ROUTE_PATH.home} />;
}

export default Splash;
