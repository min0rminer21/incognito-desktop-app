import { useSelector } from 'react-redux';
import { txHistorySendListSelector } from '../../state/txHistory/selector';

function TxHistory() {
  const txHistorySend = useSelector(txHistorySendListSelector);
  return (
    <div>
      {txHistorySend.map((history) => (
        <div key={history.hash}>
          <span>Hash: {history.hash}</span>
          <span>
            Amount:{' '}
            {history.tokenAmount.toFixed(history.tokenAmount.token.pDecimals)}{' '}
            {history.tokenAmount.token.symbol}
          </span>
        </div>
      ))}
    </div>
  );
}

export default TxHistory;
