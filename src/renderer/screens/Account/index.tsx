import { useSelector } from 'react-redux';
import { Typography } from 'antd';
import {
  accountListSelector,
  activeAccountSelector,
} from '../../state/account/selector';
import CreateAccount from './CreateAcount';
import ImportAccount from './ImportAccount';
import ListAccount from './ListAccount';
import styles from './styles.scss';

function Account() {
  const account = useSelector(activeAccountSelector);
  const accounts = useSelector(accountListSelector);

  return (
    <div className={styles.container}>
      {accounts.length ? (
        <>
          <div className={styles.actions}>
            <CreateAccount className={styles.action} />
            <ImportAccount className={styles.action} />
          </div>
          <ListAccount accounts={accounts} activeAccountName={account?.name} />
        </>
      ) : (
        <div className={styles.noAccountBox}>
          <Typography.Text className={styles.desc}>
            You have no account here. Just import or create new to start.
          </Typography.Text>
          <div className={styles.actions}>
            <CreateAccount className={styles.action} />
            <ImportAccount className={styles.action} />
          </div>
        </div>
      )}
    </div>
  );
}

export default Account;
