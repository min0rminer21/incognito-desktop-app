import { Modal, Button, Input, message } from 'antd';
import { useState } from 'react';
import useAccount from '../../../hooks/useAccount';
import { IProps } from '../CreateAcount/types';

function ImportAccount({ className }: IProps) {
  const [isImporting, setIsImporting] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [inputPK, setInputPK] = useState<string>('');
  const [inputAccountName, setInputAccountName] = useState<string>('');
  const { importAccount } = useAccount();

  const handleImportAccount = async () => {
    try {
      if (!inputPK || !inputAccountName) {
        message.error('Enter account name & private key');
        return;
      }

      setIsImporting(true);
      await importAccount(inputAccountName, inputPK);
      setIsModalVisible(false);
      setInputAccountName('');
      setInputPK('');
      message.success('Completed');
    } catch (e: any) {
      message.error(`Unable to import account: ${e.message}`);
    } finally {
      setIsImporting(false);
    }
  };

  return (
    <div className={className}>
      <Button type="primary" onClick={() => setIsModalVisible(true)}>
        Import
      </Button>
      <Modal
        title="Import account"
        visible={isModalVisible}
        onOk={handleImportAccount}
        onCancel={() => setIsModalVisible(false)}
        confirmLoading={isImporting}
        okText="Import"
      >
        <Input
          style={{ marginBottom: 10 }}
          placeholder="Enter account name"
          onChange={(e) => setInputAccountName(e.target.value)}
          value={inputAccountName}
        />
        <Input
          placeholder="Enter account private key"
          onChange={(e) => setInputPK(e.target.value)}
          value={inputPK}
        />
      </Modal>
    </div>
  );
}

export default ImportAccount;
