import {
  Collapse,
  List,
  Button,
  Typography,
  message,
  Popconfirm,
  Alert,
} from 'antd';
import useAccount from '../../../hooks/useAccount';
import { EllipsisMiddle } from '../../../components/EllipsisMiddle';
import { IListAccountProps, IAccount } from './types';

const { Panel } = Collapse;

const parseDataSource = (account: IAccount) => {
  const data = [
    { label: 'Payment Address', data: account.paymentAddress },
    { label: 'Public Key', data: account.publicKey },
    { label: 'Read Only Key', data: account.readOnlyKey },
    { label: 'OTA Private Key', data: account.otaPrivateKey },
    { label: 'Mining Key', data: account.miningKey },
    { label: 'Mining Public Key', data: account.miningPublicKey },
    { label: 'Private Key', data: account.privateKey },
  ];

  return data;
};

function ListAccount(props: IListAccountProps) {
  const { accounts } = props;
  const { setActiveAccount, activeAccount, deleteAccount } = useAccount();

  const handleDeleteAccount = async (accountName: string) => {
    try {
      await deleteAccount(accountName);
      message.success('Completed');
    } catch (e: any) {
      message.error(`Unable to delete account: ${e.message}`);
    }
  };

  return (
    <>
      <Alert
        style={{ marginBottom: 10 }}
        description="Make sure your all accounts have been backed up (the wallet does not support it)"
        type="warning"
        showIcon
        banner
      />
      <Collapse accordion>
        {accounts.map((account) => (
          <Panel
            header={account.name}
            key={account.name}
            extra={
              activeAccount?.name !== account.name ? (
                <Button
                  onClick={(e) => {
                    e.stopPropagation();
                    setActiveAccount(account.name);
                  }}
                >
                  Set active
                </Button>
              ) : (
                <Typography.Text
                  style={{ width: 96, display: 'block', textAlign: 'center' }}
                  type="success"
                >
                  Activated
                </Typography.Text>
              )
            }
          >
            <List
              itemLayout="vertical"
              dataSource={parseDataSource(account)}
              renderItem={(item) => (
                <List.Item>
                  <List.Item.Meta
                    title={item.label}
                    description={
                      <EllipsisMiddle copyable suffixCount={20}>
                        {item.data}
                      </EllipsisMiddle>
                    }
                  />
                </List.Item>
              )}
            />
            <Popconfirm
              title="Delete this account?"
              onConfirm={() => handleDeleteAccount(account.name)}
              okText="Yes"
              cancelText="No"
            >
              <Typography.Text type="danger" style={{ cursor: 'pointer' }}>
                Delete account
              </Typography.Text>
            </Popconfirm>
          </Panel>
        ))}
      </Collapse>
    </>
  );
}

export default ListAccount;
