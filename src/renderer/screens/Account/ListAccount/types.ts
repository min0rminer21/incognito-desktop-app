export interface IAccount {
  name: string;
  privateKey: string;
  paymentAddress: string;
  publicKey: string;
  readOnlyKey: string;
  otaPrivateKey: string;
  miningKey: string;
  miningPublicKey: string;
}

export interface IListAccountProps {
  accounts: IAccount[];
  activeAccountName?: string;
}
