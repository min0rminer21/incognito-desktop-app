import { Modal, Button, Input, message } from 'antd';
import { useState } from 'react';
import useAccount from '../../../hooks/useAccount';
import { IProps } from './types';

function CreateAccount({ className }: IProps) {
  const [isCreating, setIsCreating] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [inputName, setInputName] = useState<string>('');
  const { createAccount } = useAccount();

  const handleCreateAccount = async () => {
    try {
      if (!inputName) {
        message.error('Enter account name');
        return;
      }

      setIsCreating(true);
      await createAccount(inputName);
      setIsModalVisible(false);
      setInputName('');
      message.success('Completed');
    } catch (e: any) {
      message.error(`Unable to add new account: ${e.message}`);
    } finally {
      setIsCreating(false);
    }
  };

  return (
    <div className={className}>
      <Button type="primary" onClick={() => setIsModalVisible(true)}>
        Create
      </Button>
      <Modal
        title="New account"
        visible={isModalVisible}
        onOk={handleCreateAccount}
        onCancel={() => setIsModalVisible(false)}
        confirmLoading={isCreating}
        okText="Create"
      >
        <Input
          placeholder="Enter account name"
          onChange={(e) => setInputName(e.target.value)}
          value={inputName}
        />
      </Modal>
    </div>
  );
}

export default CreateAccount;
