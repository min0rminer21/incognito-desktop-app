import BigNumber from 'bignumber.js';
import { Token } from '../entities/Token';
import { TokenAmount } from '../entities/TokenAmount';
import { Amount } from '../entities/Amount';

/**
 * Parse a number to amount for an token
 * @param fixedAmount e.g: 0.1 PRV
 * @param token token object
 */
export const parseTokenAmount = (
  fixedAmount: string | number,
  token: Token
) => {
  return new TokenAmount(
    new Amount(Amount.parseFixedAmount(fixedAmount, token.pDecimals)),
    token
  );
};

/**
 * Same with `parseTokenAmount`, but use raw amount as param
 * @param rawAmount
 * @param token token object
 */
export const parseTokenAmountFromRaw = (
  rawAmount: string | number | BigNumber,
  token: Token
) => {
  return new TokenAmount(new Amount(Amount.parseRawAmount(rawAmount)), token);
};
