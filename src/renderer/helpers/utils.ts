export const classes = (...classNames: any) =>
  classNames.filter(Boolean).join(' ');

export const getTokenLogoUrl = (symbol: string) =>
  `https://s3.amazonaws.com/incognito-org/wallet/cryptocurrency-icons/32@2x/color/${symbol.toLowerCase()}@2x.png`;
