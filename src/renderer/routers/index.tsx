import { MemoryRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import TxHistory from '../screens/TxHistory';
import Token from '../screens/Token';
import Account from '../screens/Account';
import Splash from '../screens/Splash';
import Dashboard from '../screens/Dashboard';
import NotFound from '../screens/NotFound';
import { ROUTE_PATH } from './path';
import styles from './styles.scss';

const { Content, Sider } = Layout;

const MENU_ITEMS = [
  { label: 'Dashboard', path: ROUTE_PATH.home, activeKey: '1' },
  { label: 'Accounts', path: ROUTE_PATH.account, activeKey: '2' },
  { label: 'Assets', path: ROUTE_PATH.token, activeKey: '3' },
  { label: 'Transactions', path: ROUTE_PATH.txHistory, activeKey: '4' },
];

function AppRouter() {
  return (
    <Router>
      <Layout className={styles.container}>
        <Sider>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            {MENU_ITEMS.map(({ label, path, activeKey }) => (
              <Menu.Item key={activeKey}>
                {' '}
                <Link key={label} to={path}>
                  {label}
                </Link>
              </Menu.Item>
            ))}
          </Menu>
        </Sider>
        <Layout className={styles.content}>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            <Switch>
              <Route exact path={ROUTE_PATH.splash} component={Splash} />
              <Route exact path={ROUTE_PATH.home} component={Dashboard} />
              <Route exact path={ROUTE_PATH.account} component={Account} />
              <Route exact path={ROUTE_PATH.token} component={Token} />
              <Route exact path={ROUTE_PATH.txHistory} component={TxHistory} />
              <Route path="*" component={NotFound} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </Router>
  );
}

export default AppRouter;
