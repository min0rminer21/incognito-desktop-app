export const ROUTE_PATH = {
  splash: '/',
  home: '/home',
  account: '/account',
  token: '/token',
  txHistory: '/txhistory',
};
