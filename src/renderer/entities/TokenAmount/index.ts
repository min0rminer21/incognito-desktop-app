import { Amount } from '../Amount';
import { Token } from '../Token';

export class TokenAmount {
  private amount: Amount;

  token: Token;

  constructor(amount: Amount, token: Token) {
    this.amount = amount;
    this.token = token;
  }

  raw() {
    return this.amount.raw;
  }

  toFixed(decimalPlace: number) {
    return this.amount.toFixed(this.token.pDecimals, decimalPlace);
  }
}
