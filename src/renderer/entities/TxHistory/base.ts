import { TypeTxHistoryType } from './types';

export abstract class TxHistory {
  hash: string;

  fromAddress: string;

  createdAt: Date;

  txType: TypeTxHistoryType;

  constructor(hash: string, fromAddress: string) {
    this.hash = hash;
    this.fromAddress = fromAddress;
    this.createdAt = new Date();
    this.txType = 'UNKNOWN';
  }

  abstract toObject(): any;
}
