import { TokenAmount } from '../TokenAmount';
import { TxHistory } from './base';
import { ITxHistorySendObject } from './types';

export class TxHistorySend extends TxHistory {
  toAddress: string;

  tokenAmount: TokenAmount;

  constructor(data: {
    hash: string;
    fromAddress: string;
    toAddress: string;
    tokenAmount: TokenAmount;
    createdAt?: Date;
  }) {
    const { hash, fromAddress, toAddress, tokenAmount, createdAt } = data;
    super(hash, fromAddress);
    this.toAddress = toAddress;
    this.tokenAmount = tokenAmount;
    this.txType = 'SEND';
    this.createdAt = createdAt ?? this.createdAt;
  }

  toObject(): ITxHistorySendObject {
    return {
      hash: this.hash,
      fromAddress: this.fromAddress,
      toAddress: this.toAddress,
      txType: this.txType,
      createdAt: this.createdAt.toISOString(),
      rawAmount: this.tokenAmount.raw().toString(),
      tokenId: this.tokenAmount.token.tokenId,
      tokenName: this.tokenAmount.token.name,
      tokenPdecimals: this.tokenAmount.token.pDecimals,
      tokenSymbol: this.tokenAmount.token.symbol,
      tokenCurrencyType: this.tokenAmount.token.currencyType,
      tokenType: this.tokenAmount.token.type,
      tokenDecimals: this.tokenAmount.token.decimals,
      tokenPSymbol: this.tokenAmount.token.pSymbol,
      tokenVerified: this.tokenAmount.token.verified,
    };
  }
}
