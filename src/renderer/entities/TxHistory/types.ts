export type TypeTxHistoryType = 'UNKNOWN' | 'SEND';

export interface ITxHistorySendObject {
  hash: string;
  fromAddress: string;
  toAddress: string;
  txType: string;
  createdAt: string;
  rawAmount: string;
  tokenId: string;
  tokenName: string;
  tokenSymbol: string;
  tokenPSymbol: string;
  tokenPdecimals: number;
  tokenCurrencyType: number;
  tokenType: number;
  tokenVerified: boolean;
  tokenDecimals: number;
}
