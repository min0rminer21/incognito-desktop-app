import BigNumber from 'bignumber.js';

export type TypeAmount = BigNumber | string | number;

export class Amount {
  raw: BigNumber;

  /**
   * @param amount raw amount (integer & type of string, number, BigNumber)
   */
  constructor(amount: TypeAmount) {
    this.raw = Amount.parseRawAmount(amount);
  }

  static parseRawAmount(amount: TypeAmount) {
    let parsedAmount;
    if (BigNumber.isBigNumber(amount)) {
      parsedAmount = amount;
    }

    if (typeof amount === 'string' || typeof amount === 'number') {
      parsedAmount = new BigNumber(amount);
    }

    if (parsedAmount?.isInteger()) {
      return parsedAmount;
    }

    throw new Error(
      'Invalid amount: must be an integer & type of string, number or a BigNumber'
    );
  }

  static parseFixedAmount(fixedAmount: string | number, pDecimals: number) {
    return Amount.parseRawAmount(
      new BigNumber(fixedAmount).multipliedBy(new BigNumber(10).pow(pDecimals))
    );
  }

  toFixed(pDecimals: number, decimalPlace: number) {
    return this.raw
      .dividedBy(new BigNumber(10).pow(pDecimals))
      .toFixed(decimalPlace);
  }
}
