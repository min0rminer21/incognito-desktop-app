export class Token {
  name: string;

  symbol: string;

  pSymbol: string;

  currencyType: number;

  type: number;

  verified: boolean;

  tokenId: string;

  pDecimals: number;

  decimals: number;

  contractID?: string;

  priceUsd?: number;

  pricePrv?: number;

  constructor(data: {
    name: string;

    symbol: string;

    pSymbol: string;

    currencyType: number;

    type: number;

    contractID?: string;

    verified: boolean;

    priceUsd?: number;

    pricePrv?: number;

    tokenId: string;

    pDecimals: number;

    decimals: number;
  }) {
    this.name = data.name;
    this.symbol = data.symbol;
    this.tokenId = data.tokenId;
    this.pDecimals = data.pDecimals;
    this.pSymbol = data.pSymbol;
    this.contractID = data.contractID;
    this.currencyType = data.currencyType;
    this.type = data.type;
    this.decimals = data.decimals;
    this.pricePrv = data.pricePrv;
    this.priceUsd = data.priceUsd;
    this.verified = data.verified;
  }

  get typeName(): 'Coin' | 'Token' | '' {
    switch (this.type) {
      case 0:
        return 'Coin';
      case 1:
        return 'Token';
      default:
        return '';
    }
  }

  get networkName():
    | 'Incognito'
    | 'ERC20'
    | 'BEP20'
    | 'BEP2'
    | 'Incognito'
    | 'Ethereum'
    | 'Binance'
    | 'Binance Smart Chain'
    | 'Bitcoin'
    | 'Tomochain'
    | 'USD'
    | 'ZIL'
    | 'XMR'
    | 'NEO'
    | 'DASH'
    | 'LTC'
    | 'DOGE'
    | 'ZEC'
    | 'DOT'
    | '' {
    switch (this.currencyType) {
      case 0:
        return 'Incognito';
      case 1:
        return 'Ethereum';
      case 2:
        return 'Bitcoin';
      case 3:
        return 'ERC20';
      case 4:
        return 'Binance';
      case 5:
        return 'BEP2';
      case 6:
        return 'USD';
      case 7:
        return 'Binance Smart Chain';
      case 8:
        return 'BEP20';
      case 9:
        return 'Tomochain';
      case 10:
        return 'ZIL';
      case 11:
        return 'XMR';
      case 12:
        return 'NEO';
      case 13:
        return 'DASH';
      case 14:
        return 'LTC';
      case 15:
        return 'DOGE';
      case 16:
        return 'ZEC';
      case 17:
        return 'DOT';
      default:
        return '';
    }
  }
}
