import { IAccountObject } from './types';

export class Account {
  name: string;

  privateKey: string;

  paymentAddress: string;

  miningKey: string;

  miningPublicKey: string;

  otaPrivateKey: string;

  publicKey: string;

  readOnlyKey: string;

  constructor(data: {
    name: string;
    privateKey: string;
    paymentAddress: string;
    miningKey: string;
    miningPublicKey: string;
    otaPrivateKey: string;
    publicKey: string;
    readOnlyKey: string;
  }) {
    const {
      name,
      privateKey,
      paymentAddress,
      miningKey,
      miningPublicKey,
      otaPrivateKey,
      publicKey,
      readOnlyKey,
    } = data;
    this.name = name;
    this.privateKey = privateKey;
    this.paymentAddress = paymentAddress;
    this.miningKey = miningKey;
    this.miningPublicKey = miningPublicKey;
    this.otaPrivateKey = otaPrivateKey;
    this.publicKey = publicKey;
    this.readOnlyKey = readOnlyKey;
  }

  static isValidAccountObject(data: IAccountObject) {
    if (
      [
        data.name,
        data.privateKey,
        data.paymentAddress,
        data.miningKey,
        data.miningPublicKey,
        data.otaPrivateKey,
        data.publicKey,
        data.readOnlyKey,
      ].every(Boolean)
    ) {
      return true;
    }

    return false;
  }

  static fromObject(data: IAccountObject) {
    return new Account({
      name: data.name,
      privateKey: data.privateKey,
      paymentAddress: data.paymentAddress,
      miningKey: data.miningKey,
      miningPublicKey: data.miningPublicKey,
      otaPrivateKey: data.otaPrivateKey,
      publicKey: data.publicKey,
      readOnlyKey: data.readOnlyKey,
    });
  }

  toObject(): IAccountObject {
    return {
      name: this.name,
      privateKey: this.privateKey,
      paymentAddress: this.paymentAddress,
      miningKey: this.miningKey,
      miningPublicKey: this.miningPublicKey,
      otaPrivateKey: this.otaPrivateKey,
      publicKey: this.publicKey,
      readOnlyKey: this.readOnlyKey,
    };
  }
}
