export interface IAccountObject {
  name: string;
  privateKey: string;
  paymentAddress: string;
  miningKey: string;
  miningPublicKey: string;
  otaPrivateKey: string;
  publicKey: string;
  readOnlyKey: string;
}
