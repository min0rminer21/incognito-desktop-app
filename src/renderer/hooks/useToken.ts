import { useCallback } from 'react';
import { useSelector } from 'react-redux';
import {
  addTokenFavoriteAction,
  removeTokenFavoriteAction,
} from '../state/token/action';
import { parseTokenAmountFromRaw } from '../helpers/balance';
import { getBalance } from '../services/incognitoCli';
import { Token } from '../entities/Token';
import { ptokenSetSelector } from '../state/app/selector';
import { favoriteTokenSelector } from '../state/token/selector';
import { useAppDispatch } from '../state/store';

const useToken = () => {
  const dispatch = useAppDispatch();
  const pTokenSet = useSelector(ptokenSetSelector);
  const favTokens = useSelector(favoriteTokenSelector);

  const getToken = useCallback(
    (tokenId: string) => {
      const rawTokenData = pTokenSet[tokenId];
      if (rawTokenData) {
        return new Token({
          name: rawTokenData.name,
          symbol: rawTokenData.symbol,
          pSymbol: rawTokenData.pSymbol,
          decimals: rawTokenData.decimals,
          pDecimals: rawTokenData.pDecimals,
          currencyType: rawTokenData.currencyType,
          type: rawTokenData.type,
          contractID: rawTokenData.contractID,
          verified: rawTokenData.verified,
          priceUsd: rawTokenData.priceUsd,
          pricePrv: rawTokenData.pricePrv,
          tokenId: rawTokenData.tokenId,
        });
      }

      return undefined;
    },
    [pTokenSet]
  );

  const getFavTokens = useCallback(() => {
    return favTokens.filter(Boolean).map(
      (rawTokenData) =>
        new Token({
          name: rawTokenData.name,
          symbol: rawTokenData.symbol,
          pSymbol: rawTokenData.pSymbol,
          decimals: rawTokenData.decimals,
          pDecimals: rawTokenData.pDecimals,
          currencyType: rawTokenData.currencyType,
          type: rawTokenData.type,
          contractID: rawTokenData.contractID,
          verified: rawTokenData.verified,
          priceUsd: rawTokenData.priceUsd,
          pricePrv: rawTokenData.pricePrv,
          tokenId: rawTokenData.tokenId,
        })
    );
  }, [favTokens]);

  const getAllTokenList = useCallback(() => {
    return Object.values(pTokenSet)
      .filter(Boolean)
      .map(
        (rawTokenData) =>
          new Token({
            name: rawTokenData.name,
            symbol: rawTokenData.symbol,
            pSymbol: rawTokenData.pSymbol,
            decimals: rawTokenData.decimals,
            pDecimals: rawTokenData.pDecimals,
            currencyType: rawTokenData.currencyType,
            type: rawTokenData.type,
            contractID: rawTokenData.contractID,
            verified: rawTokenData.verified,
            priceUsd: rawTokenData.priceUsd,
            pricePrv: rawTokenData.pricePrv,
            tokenId: rawTokenData.tokenId,
          })
      );
  }, [pTokenSet]);

  const getTokenBalance = useCallback(
    (tokenId: string, privateKey: string) => {
      const token = getToken(tokenId);

      if (token) {
        return getBalance(privateKey, tokenId).then((rawBalance) => {
          return parseTokenAmountFromRaw(rawBalance, token);
        });
      }
      throw new Error('Token is invalid');
    },
    [getToken]
  );

  const setFavToken = useCallback(
    (tokenId: string) => {
      dispatch(addTokenFavoriteAction(tokenId));
    },
    [dispatch]
  );

  const deleteFavToken = useCallback(
    (tokenId: string) => {
      dispatch(removeTokenFavoriteAction(tokenId));
    },
    [dispatch]
  );

  return {
    getToken,
    getTokenBalance,
    getFavTokens,
    getAllTokenList,
    setFavToken,
    deleteFavToken,
  };
};

export default useToken;
