import { useDispatch } from 'react-redux';
import { addSendTxHistoryAction } from '../state/txHistory/action';
import { TokenAmount } from '../entities/TokenAmount';
import { TxHistorySend } from '../entities/TxHistory/send';

const useTxHistory = () => {
  const dispatch = useDispatch();
  const saveSendHistory = (
    hash: string,
    fromAddress: string,
    toAddress: string,
    tokenAmount: TokenAmount
  ) => {
    const history = new TxHistorySend({
      hash,
      fromAddress,
      toAddress,
      tokenAmount,
    });

    dispatch(addSendTxHistoryAction(history));
    return history;
  };

  return { saveSendHistory };
};

export default useTxHistory;
