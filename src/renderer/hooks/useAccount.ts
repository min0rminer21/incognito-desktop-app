import { useSelector } from 'react-redux';
import { useCallback } from 'react';
import { useAppDispatch } from '../state/store';
import {
  createAccountAction,
  deleteAccountAction,
  importAccountAction,
  selectAccountAction,
} from '../state/account/action';
import {
  accountSetSelector,
  activeAccountSelector,
} from '../state/account/selector';
import { Account } from '../entities/Account';

const useAccount = () => {
  const dispatch = useAppDispatch();
  const accountRaw = useSelector(activeAccountSelector);
  const accountSet = useSelector(accountSetSelector);
  const activeAccount =
    accountRaw &&
    new Account({
      name: accountRaw.name,
      privateKey: accountRaw.privateKey,
      paymentAddress: accountRaw.paymentAddress,
      miningKey: accountRaw.miningKey,
      miningPublicKey: accountRaw.miningPublicKey,
      otaPrivateKey: accountRaw.otaPrivateKey,
      publicKey: accountRaw.publicKey,
      readOnlyKey: accountRaw.readOnlyKey,
    });

  const createAccount = useCallback(
    (accountName: string) => {
      if (accountSet[accountName]) {
        throw new Error('Account is existed');
      }
      return dispatch(createAccountAction(accountName)).unwrap();
    },
    [accountSet, dispatch]
  );

  const importAccount = useCallback(
    (accountName: string, privateKey: string) => {
      if (
        accountSet[accountName] ||
        Object.values(accountSet).find(
          (account) => account.privateKey === privateKey
        )
      ) {
        throw new Error('Account is existed');
      }
      return dispatch(
        importAccountAction({ name: accountName, privateKey })
      ).unwrap();
    },
    [accountSet, dispatch]
  );

  const setActiveAccount = useCallback(
    (accountName: string) => {
      return dispatch(selectAccountAction(accountName));
    },
    [dispatch]
  );

  const deleteAccount = useCallback(
    (accountName: string) => {
      return dispatch(deleteAccountAction(accountName));
    },
    [dispatch]
  );

  return {
    createAccount,
    importAccount,
    setActiveAccount,
    deleteAccount,
    activeAccount,
  };
};

export default useAccount;
