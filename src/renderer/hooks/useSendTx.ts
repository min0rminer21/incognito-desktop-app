import { useDispatch } from 'react-redux';
import { Account } from '../entities/Account';
import { updateTransactionStatusAction } from '../state/transaction/action';
import { TokenAmount } from '../entities/TokenAmount';
import { send } from '../services/incognitoCli';
import useTxHistory from './useTxHistory';

const useSendTx = () => {
  const dispatch = useDispatch();
  const { saveSendHistory } = useTxHistory();
  const sendTx = async (
    account: Account,
    address: string,
    tokenAmount: TokenAmount
  ) => {
    dispatch(updateTransactionStatusAction('SENDING'));
    const txHash = await send(
      account.privateKey,
      address,
      tokenAmount.raw(),
      tokenAmount.token.tokenId
    );
    dispatch(updateTransactionStatusAction('NONE'));

    // save history
    saveSendHistory(txHash, account.paymentAddress, address, tokenAmount);
    return txHash;
  };

  return { sendTx };
};

export default useSendTx;
