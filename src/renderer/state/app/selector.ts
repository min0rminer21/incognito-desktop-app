import { createDraftSafeSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectSelf = (state: RootState) => state;

export const ptokenSetSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.app.ptokenSet
);
