import { createReducer } from '@reduxjs/toolkit';
import { loadPtokenSetAction } from './action';
import { IAppState, IPTokenSet } from './types';

const initialState: IAppState = {
  ptokenSet: {},
};

export const appReducer = createReducer(initialState, (builder) => {
  builder.addCase(loadPtokenSetAction.fulfilled, (state, action) => {
    const ptokenSet: IPTokenSet = {};
    action.payload.forEach((ptokenRaw) => {
      ptokenSet[ptokenRaw.TokenID] = {
        tokenId: ptokenRaw.TokenID,
        symbol: ptokenRaw.Symbol,
        pSymbol: ptokenRaw.PSymbol,
        name: ptokenRaw.Name,
        decimals: ptokenRaw.Decimals,
        pDecimals: ptokenRaw.PDecimals,
        verified: ptokenRaw.Verified,
        contractID: ptokenRaw.ContractID,
        pricePrv: ptokenRaw.PricePrv,
        priceUsd: ptokenRaw.PriceUsd,
        currencyType: ptokenRaw.CurrencyType,
        type: ptokenRaw.Type,
      };
    });

    state.ptokenSet = ptokenSet;
  });
});
