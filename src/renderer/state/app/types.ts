export interface IPToken {
  tokenId: string;
  symbol: string;
  pSymbol: string;
  name: string;
  decimals: number;
  pDecimals: number;
  verified: boolean;
  currencyType: number;
  type: number;
  contractID?: string;
  priceUsd: number;
  pricePrv: number;
}

export interface IPTokenSet {
  [tokenId: string]: IPToken;
}

export interface IAppState {
  ptokenSet: IPTokenSet;
}
