import { createAsyncThunk } from '@reduxjs/toolkit';
import { getPTokenSet } from '../../services/api/incognito';

const SCOPE = 'APP';

export const loadPtokenSetAction = createAsyncThunk(
  `${SCOPE}/LOAD_PTOKEN_SET`,
  async () => {
    const ptokenRawData = await getPTokenSet();

    return ptokenRawData;
  }
);
