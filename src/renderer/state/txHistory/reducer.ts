import { createReducer } from '@reduxjs/toolkit';
import { addSendTxHistoryAction } from './action';
import { ITxHistoryState } from './types';

const initialState: ITxHistoryState = {
  send: {},
};

export const txHistoryReducer = createReducer(initialState, (builder) => {
  builder.addCase(addSendTxHistoryAction, (state, action) => {
    if (!state.send[action.payload.hash]) {
      state.send[action.payload.hash] = action.payload.toObject();
    }
  });
});
