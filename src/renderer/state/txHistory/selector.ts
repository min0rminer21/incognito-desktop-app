import { createDraftSafeSelector } from '@reduxjs/toolkit';
import { TxHistorySend } from '../../entities/TxHistory/send';
import { parseTokenAmountFromRaw } from '../../helpers/balance';
import { Token } from '../../entities/Token';
import { RootState } from '../store';

const selectSelf = (state: RootState) => state;

export const txHistorySendObjectSetSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.txHistory.send
);

export const txHistorySendListSelector = createDraftSafeSelector(
  txHistorySendObjectSetSelector,
  (txHistorySendObjectSet) => {
    return Object.values(txHistorySendObjectSet).map(
      (txHistorySendObject) =>
        new TxHistorySend({
          hash: txHistorySendObject.hash,
          fromAddress: txHistorySendObject.fromAddress,
          toAddress: txHistorySendObject.toAddress,
          tokenAmount: parseTokenAmountFromRaw(
            txHistorySendObject.rawAmount,
            new Token({
              name: txHistorySendObject.tokenName,
              symbol: txHistorySendObject.tokenSymbol,
              pSymbol: txHistorySendObject.tokenPSymbol,
              decimals: txHistorySendObject.tokenDecimals,
              pDecimals: txHistorySendObject.tokenPdecimals,
              currencyType: txHistorySendObject.tokenCurrencyType,
              verified: txHistorySendObject.tokenVerified,
              tokenId: txHistorySendObject.tokenId,
              type: txHistorySendObject.tokenType,
            })
          ),
        })
    );
  }
);
