import { createAction } from '@reduxjs/toolkit';
import { TxHistorySend } from '../../entities/TxHistory/send';

const SCOPE = 'TXHISTORY';

export const addSendTxHistoryAction = createAction<TxHistorySend>(
  `${SCOPE}/ADD_SEND_TX`
);
