import { ITxHistorySendObject } from '../../entities/TxHistory/types';

export interface ITxHistoryState {
  send: { [hash: string]: ITxHistorySendObject };
}
