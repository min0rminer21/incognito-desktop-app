export interface IAccount {
  privateKey: string;
  paymentAddress: string;
  publicKey: string;
  readOnlyKey: string;
  otaPrivateKey: string;
  miningKey: string;
  miningPublicKey: string;
  name: string;
}

export interface IAccountState {
  set: { [name: string]: IAccount };
  userSelectAccountName?: string;
}
