import { createDraftSafeSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectSelf = (state: RootState) => state;

export const userSelectAccountNameSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.account.userSelectAccountName
);

export const accountSetSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.account.set
);

export const accountListSelector = createDraftSafeSelector(
  selectSelf,
  (state) => Object.values(state.account.set)
);

export const activeAccountSelector = createDraftSafeSelector(
  accountSetSelector,
  userSelectAccountNameSelector,
  (accounts, userSelectAccountName) => {
    if (userSelectAccountName && accounts[userSelectAccountName]) {
      return accounts[userSelectAccountName];
    }
    // return first account
    if (Object.values(accounts).length) {
      return Object.values(accounts)[0];
    }

    return undefined;
  }
);
