import { createReducer } from '@reduxjs/toolkit';
import {
  createAccountAction,
  deleteAccountAction,
  importAccountAction,
  selectAccountAction,
} from './action';
import { IAccountState } from './types';

const initialState: IAccountState = {
  set: {},
};

export const accountReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(createAccountAction.fulfilled, (state, action) => {
      if (!state.set[action.payload.name]) {
        state.set[action.payload.name] = {
          privateKey: action.payload.privateKey,
          paymentAddress: action.payload.paymentAddress,
          publicKey: action.payload.publicKey,
          readOnlyKey: action.payload.readOnlyKey,
          otaPrivateKey: action.payload.otaPrivateKey,
          miningKey: action.payload.miningKey,
          miningPublicKey: action.payload.miningPublicKey,
          name: action.payload.name,
        };
      }
    })
    .addCase(importAccountAction.fulfilled, (state, action) => {
      if (!state.set[action.payload.name]) {
        state.set[action.payload.name] = {
          privateKey: action.payload.privateKey,
          paymentAddress: action.payload.paymentAddress,
          publicKey: action.payload.publicKey,
          readOnlyKey: action.payload.readOnlyKey,
          otaPrivateKey: action.payload.otaPrivateKey,
          miningKey: action.payload.miningKey,
          miningPublicKey: action.payload.miningPublicKey,
          name: action.payload.name,
        };
      }
    })
    .addCase(selectAccountAction, (state, action) => {
      if (action.payload && state.set[action.payload]) {
        state.userSelectAccountName = action.payload;
      }
    })
    .addCase(deleteAccountAction.fulfilled, (state, action) => {
      if (action.payload && state.set[action.payload]) {
        delete state.set[action.payload];
      }
    });
});
