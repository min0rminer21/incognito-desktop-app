import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { Account } from '../../entities/Account';
import {
  createNewAccount,
  getKeyInfo,
  submitOtaKey,
} from '../../services/incognitoCli';

const SCOPE = 'ACCOUNT';

export const createAccountAction = createAsyncThunk(
  `${SCOPE}/CREATE`,
  async (name: string) => {
    const accountRawData = await createNewAccount();
    const account = new Account({
      name,
      privateKey: accountRawData.keys.PrivateKey,
      paymentAddress: accountRawData.keys.PaymentAddress,
      miningKey: accountRawData.keys.MiningKey,
      miningPublicKey: accountRawData.keys.MiningPublicKey,
      otaPrivateKey: accountRawData.keys.OTAPrivateKey,
      publicKey: accountRawData.keys.PublicKey,
      readOnlyKey: accountRawData.keys.ReadOnlyKey,
    });

    // need to submit ota key after creating new account
    await submitOtaKey(accountRawData.keys.OTAPrivateKey);

    return account.toObject();
  }
);

export const importAccountAction = createAsyncThunk(
  `${SCOPE}/IMPORT`,
  async (data: { privateKey: string; name: string }) => {
    const accountRawData = await getKeyInfo(data.privateKey);

    const account = new Account({
      name: data.name,
      privateKey: accountRawData.PrivateKey,
      paymentAddress: accountRawData.PaymentAddress,
      miningKey: accountRawData.MiningKey,
      miningPublicKey: accountRawData.MiningPublicKey,
      otaPrivateKey: accountRawData.OTAPrivateKey,
      publicKey: accountRawData.PublicKey,
      readOnlyKey: accountRawData.ReadOnlyKey,
    });

    // need to submit ota key after importing new account
    await submitOtaKey(accountRawData.OTAPrivateKey);

    return account.toObject();
  }
);

export const deleteAccountAction = createAsyncThunk(
  `${SCOPE}/DELETE`,
  async (accountName: string) => {
    return accountName;
  }
);

export const selectAccountAction = createAction<string>(`${SCOPE}/SELECT`);
