import { createReducer } from '@reduxjs/toolkit';
import { TOKEN_ID } from '../../resources/constant';
import { addTokenFavoriteAction, removeTokenFavoriteAction } from './action';
import { ITokenState } from './types';

const initialState: ITokenState = {
  favoriteTokenIds: {
    [TOKEN_ID.PRV]: { tokenId: TOKEN_ID.PRV, default: true },
    [TOKEN_ID.BTC]: { tokenId: TOKEN_ID.BTC, default: false },
    [TOKEN_ID.ETH]: { tokenId: TOKEN_ID.ETH, default: false },
    [TOKEN_ID.XMR]: { tokenId: TOKEN_ID.XMR, default: false },
    [TOKEN_ID.USDT]: { tokenId: TOKEN_ID.USDT, default: false },
  },
};

export const tokenReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(addTokenFavoriteAction, (state, action) => {
      if (action.payload && !state.favoriteTokenIds[action.payload]) {
        state.favoriteTokenIds[action.payload] = { tokenId: action.payload };
      }
    })
    .addCase(removeTokenFavoriteAction, (state, action) => {
      if (action.payload && state.favoriteTokenIds[action.payload]) {
        delete state.favoriteTokenIds[action.payload];
      }
    });
});
