export interface IFavoriteToken {
  tokenId: string;
  default?: boolean;
}

export interface ITokenState {
  favoriteTokenIds: { [tokenId: string]: IFavoriteToken };
}
