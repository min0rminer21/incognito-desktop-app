import { createAction } from '@reduxjs/toolkit';

const SCOPE = 'TOKEN';

export const addTokenFavoriteAction = createAction<string>(
  `${SCOPE}/ADD_FAVORITE`
);

export const removeTokenFavoriteAction = createAction<string>(
  `${SCOPE}/REMOVE_FAVORITE`
);
