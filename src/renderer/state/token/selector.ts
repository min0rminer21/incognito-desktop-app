import { createDraftSafeSelector } from '@reduxjs/toolkit';
import { ptokenSetSelector } from '../app/selector';
import { RootState } from '../store';

const selectSelf = (state: RootState) => state;

export const favoriteTokenIdsSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.token.favoriteTokenIds
);

export const favoriteTokenSelector = createDraftSafeSelector(
  favoriteTokenIdsSelector,
  ptokenSetSelector,
  (favoriteIds, ptokenSet) => {
    return Object.values(favoriteIds)
      .map((favTokenMeta) => {
        return ptokenSet[favTokenMeta.tokenId];
      })
      .filter(Boolean);
  }
);
