export type TypeTransactionStatus = 'NONE' | 'SENDING';

export interface ITransactionState {
  status: TypeTransactionStatus;
}
