import { createDraftSafeSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectSelf = (state: RootState) => state;

export const transactionStatusSelector = createDraftSafeSelector(
  selectSelf,
  (state) => state.transaction.status
);
