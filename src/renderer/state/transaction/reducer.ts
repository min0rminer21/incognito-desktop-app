import { createReducer } from '@reduxjs/toolkit';
import { updateTransactionStatusAction } from './action';
import { ITransactionState } from './types';

const initialState: ITransactionState = {
  status: 'NONE',
};

export const transactionReducer = createReducer(initialState, (builder) => {
  builder.addCase(updateTransactionStatusAction, (state, action) => {
    state.status = action.payload;
  });
});
