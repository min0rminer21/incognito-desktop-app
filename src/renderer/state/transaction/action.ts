import { createAction } from '@reduxjs/toolkit';
import { TypeTransactionStatus } from './types';

const SCOPE = 'TRANSACTION';

export const updateTransactionStatusAction =
  createAction<TypeTransactionStatus>(`${SCOPE}/UPDATE_STATUS`);
