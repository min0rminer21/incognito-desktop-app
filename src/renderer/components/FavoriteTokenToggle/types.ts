export interface IProps {
  tokenId: string;
  onAdded?: () => void;
}
