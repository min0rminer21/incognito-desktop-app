import { StarOutlined } from '@ant-design/icons';
import { blue, grey } from '@ant-design/colors';
import { message, Popconfirm, Typography, Tooltip } from 'antd';
import useToken from '../../hooks/useToken';
import SupportPost from '../SupportPost';
import { IProps } from './types';
import styles from './styles.scss';

function FavoriteTokenToggle({ tokenId, onAdded }: IProps) {
  const { setFavToken, deleteFavToken, getToken, getFavTokens } = useToken();
  const token = getToken(tokenId);
  const favToken = getFavTokens();
  const isMarkedFav = !!favToken.find((t) => t.tokenId === tokenId);

  const handleMarkFav = () => {
    setFavToken(tokenId);
    message.success('Marked as favorite');
    onAdded?.();
  };

  const renderDeleteFav = () => {
    return (
      <Tooltip title="Remove favorite" mouseEnterDelay={0.3}>
        <StarOutlined
          style={{ color: blue.primary, fontSize: 18 }}
          onClick={() => deleteFavToken(tokenId)}
        />
      </Tooltip>
    );
  };

  const renderAddFav = () => {
    if (token?.verified) {
      return (
        <Tooltip title="Mark as favorite" mouseEnterDelay={1}>
          <StarOutlined
            style={{ color: grey.primary, fontSize: 18 }}
            onClick={handleMarkFav}
          />
        </Tooltip>
      );
    }

    return (
      <Popconfirm
        title={
          <Typography.Text>
            This token is unverified. Still mark as favorite?
            <SupportPost post="VERIFIED_TOKEN_BADGE" />
          </Typography.Text>
        }
        onConfirm={handleMarkFav}
        okText="Yes"
        cancelText="No"
      >
        <Tooltip title="Mark as favorite" mouseEnterDelay={1}>
          <StarOutlined style={{ color: grey.primary, fontSize: 18 }} />
        </Tooltip>
      </Popconfirm>
    );
  };

  if (!token) return null;

  return (
    <div className={styles.container}>
      {isMarkedFav ? renderDeleteFav() : renderAddFav()}
    </div>
  );
}

export default FavoriteTokenToggle;
