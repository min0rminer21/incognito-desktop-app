import { useEffect, useState } from 'react';
import { message, Typography, Spin } from 'antd';
import { TokenAmount } from '../../entities/TokenAmount';
import useToken from '../../hooks/useToken';
import useAccount from '../../hooks/useAccount';
import { IProps } from './types';
import styles from './styles.scss';

function TokenBalance(props: IProps) {
  const { tokenId } = props;
  const { activeAccount } = useAccount();
  const [balance, setBalance] = useState<TokenAmount | undefined>(undefined);
  const [isLoadingBalance, setIsLoadingBalance] = useState(false);
  const { getTokenBalance } = useToken();

  useEffect(() => {
    if (tokenId && activeAccount?.privateKey) {
      setIsLoadingBalance(true);
      getTokenBalance(tokenId, activeAccount.privateKey)
        .then(setBalance)
        .catch((e: any) => {
          message.error(`Unable to get balance: ${e.message}`);
        })
        .finally(() => {
          setIsLoadingBalance(false);
        });
    }
  }, [tokenId, activeAccount?.privateKey, getTokenBalance]);

  return (
    <div className={styles.container}>
      {isLoadingBalance && <Spin size="small" />}
      <div className={styles.balanceBox}>
        <Typography.Title level={4}>
          {balance?.toFixed(balance.token.pDecimals)}
        </Typography.Title>
        {balance?.token.priceUsd && (
          <Typography.Text>
            (~
            {` `}
            {(
              balance.token.priceUsd *
              Number(balance.toFixed(balance.token.pDecimals))
            ).toFixed(2)}
            {` `}
            USD)
          </Typography.Text>
        )}
      </div>
    </div>
  );
}

export default TokenBalance;
