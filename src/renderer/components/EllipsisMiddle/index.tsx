import { Typography } from 'antd';

const { Text } = Typography;

export const EllipsisMiddle = ({
  suffixCount,
  children,
  copyable = false,
}: {
  suffixCount: number;
  children: string;
  copyable?: boolean;
}) => {
  // const start = children.slice(0, children.length - suffixCount).trim();
  const suffix = children.slice(-suffixCount).trim();
  return (
    <Text
      copyable={copyable}
      style={{ maxWidth: '100%' }}
      ellipsis={{ suffix }}
    >
      {children}
    </Text>
  );
};

EllipsisMiddle.defaultProps = {
  copyable: false,
};
