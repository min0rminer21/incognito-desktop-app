import { Avatar, Card } from 'antd';
import { getTokenLogoUrl } from '../../helpers/utils';
import { IProps } from './types';

function Token(props: IProps) {
  const { token } = props;
  return (
    <Card.Meta
      avatar={<Avatar src={getTokenLogoUrl(token.symbol)} />}
      title={`${token.name} (${token.symbol})`}
      description={token.networkName}
    />
  );
}

export default Token;
