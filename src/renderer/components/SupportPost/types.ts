export type TypeSuportPost = 'VERIFIED_TOKEN_BADGE';

export interface IProps {
  post: TypeSuportPost;
  className?: string;
}
