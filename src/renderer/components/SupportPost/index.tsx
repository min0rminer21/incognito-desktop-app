import { classes } from '../../helpers/utils';
import { IProps, TypeSuportPost } from './types';
import styles from './styles.scss';

const getPostInfo = (post: TypeSuportPost) => {
  switch (post) {
    case 'VERIFIED_TOKEN_BADGE':
      return {
        label: 'More info',
        url: 'https://we.incognito.org/t/verified-badges-for-custom-privacy-coins-on-incognito-chain/952',
      };
    default:
      return undefined;
  }
};

function SupportPost(props: IProps) {
  const { post, className } = props;
  const info = getPostInfo(post);

  if (!info) return null;

  return (
    <a
      className={classes(className, styles.container)}
      target="_blank"
      href={info.url}
      rel="noreferrer"
    >
      {info.label}
    </a>
  );
}

export default SupportPost;
