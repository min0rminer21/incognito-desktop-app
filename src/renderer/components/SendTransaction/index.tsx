import { Button, Input, Modal, Form, message } from 'antd';
import { useState } from 'react';
import { parseTokenAmount } from '../../helpers/balance';
import { Account } from '../../entities/Account';
import { TokenAmount } from '../../entities/TokenAmount';
import useAccount from '../../hooks/useAccount';
import useSendTx from '../../hooks/useSendTx';
import useToken from '../../hooks/useToken';
import { IProps } from './types';

function SendTransaction({ tokenId }: IProps) {
  const [isShowForm, setIsShowForm] = useState(false);
  const [isSending, setIsSending] = useState(false);
  const { activeAccount } = useAccount();
  const { getToken } = useToken();
  const token = getToken(tokenId);
  const { sendTx } = useSendTx();

  const handleSend = async (
    account: Account,
    toAddress: string,
    tokenAmount: TokenAmount
  ) => {
    try {
      setIsSending(true);
      if (account && tokenAmount && toAddress) {
        const txHash = await sendTx(account, toAddress, tokenAmount);
        message.success('Sent');
        setIsShowForm(false);
        return txHash;
      }

      throw new Error('Missing data');
    } catch (e: any) {
      message.error(`Unable to send transaction: ${e.message}`);
      return undefined;
    } finally {
      setIsSending(false);
    }
  };

  const onFinish = (values: any) => {
    const { toAddress, amount } = values || {};

    if (activeAccount && toAddress && amount && token) {
      const tokenAmount = parseTokenAmount(amount, token);
      handleSend(activeAccount, toAddress, tokenAmount);
    } else {
      message.error('Missing data to send transaction');
    }
  };

  if (!token) return null;

  return (
    <div>
      <Button type="primary" onClick={() => setIsShowForm(true)}>
        Send
      </Button>
      <Modal
        title={`Send ${token.name} (${token.symbol})`}
        visible={isShowForm}
        footer={null}
        onCancel={() => setIsShowForm(false)}
      >
        <Form
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="To Incognito address"
            name="toAddress"
            rules={[
              {
                required: true,
                message: 'Please input Incognito address',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Amount"
            name="amount"
            rules={[
              {
                required: true,
                message: 'Invalid amount',
              },
            ]}
          >
            <Input type="number" />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button
              type="default"
              onClick={() => setIsShowForm(false)}
              style={{ marginRight: 30 }}
            >
              Cancel
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              loading={isSending}
              disabled={isSending}
            >
              Send
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default SendTransaction;
