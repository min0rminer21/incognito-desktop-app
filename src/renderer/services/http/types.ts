import type { AxiosRequestConfig } from 'axios';

type RequestConfig = AxiosRequestConfig;

declare module 'axios' {
  export interface AxiosInstance {
    request<T>(config: RequestConfig): Promise<T>;
    get<T>(url: string, config?: RequestConfig): Promise<T>;
    delete<T>(url: string, config?: RequestConfig): Promise<T>;
    head<T>(url: string, config?: RequestConfig): Promise<T>;
    post<T>(url: string, data?: any, config?: RequestConfig): Promise<T>;
    put<T>(url: string, data?: any, config?: RequestConfig): Promise<T>;
    patch<T>(url: string, data?: any, config?: RequestConfig): Promise<T>;
  }
}
