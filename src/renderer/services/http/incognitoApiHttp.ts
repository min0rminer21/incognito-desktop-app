import axios from 'axios';
import { API_SERVICE_INCOGNITO_URL } from '../../resources/config';

const incognitoApiHttp = axios.create({
  timeout: 10 * 1000,
  baseURL: API_SERVICE_INCOGNITO_URL,
});

incognitoApiHttp.interceptors.response.use((res) => {
  const result = res.data.Result;
  const error = res.data.Error;

  if (error) {
    throw new Error(error.Message);
  }
  return result;
});

export default incognitoApiHttp;
