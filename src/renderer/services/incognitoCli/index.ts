import BigNumber from 'bignumber.js';
import { IncognitoCliMethods } from '../../../common/types/incognitoCliMethod';

const incognitoCliExposed: IncognitoCliMethods = (window as any).electron
  .incognitoCli;

/**
 * Generate a new mnemonic phrase and its Incognito account.
 * https://github.com/incognitochain/incognito-cli#generateaccount
 */
export const createNewAccount = () => {
  return incognitoCliExposed.createNewAccount().catch(() => {
    throw new Error('CLI error');
  });
};

/**
 * Get all related-keys of a private key.
 * https://github.com/incognitochain/incognito-cli#keyinfo
 */
export const getKeyInfo = (privateKey: string) => {
  return incognitoCliExposed.getKeyInfo(privateKey).catch(() => {
    throw new Error('CLI error');
  });
};

/**
 * Submit an ota key to the full-node.
 * https://github.com/incognitochain/incognito-cli#submitkey
 */
export const submitOtaKey = (otaKey: string) => {
  return incognitoCliExposed.submitOtaKey(otaKey).catch(() => {
    throw new Error('CLI error');
  });
};

/**
 * Check the balance of an account.
 * https://github.com/incognitochain/incognito-cli#balance
 */
export const getBalance = async (privateKey: string, tokenID?: string) => {
  try {
    const balanceString = await incognitoCliExposed.getBalance({
      privateKey,
      tokenID,
    });

    return new BigNumber(balanceString);
  } catch (e) {
    throw new Error('CLI error');
  }
};

/**
 * Send an amount of PRV or token from one wallet to another wallet.
 * https://github.com/incognitochain/incognito-cli/tree/main#send
 * @returns tx id (string)
 */
export const send = (
  privateKey: string,
  address: string,
  amount: BigNumber,
  tokenID?: string
) => {
  return incognitoCliExposed
    .send({
      privateKey,
      tokenID,
      address,
      amountStr: amount.toString(),
    })
    .catch(() => {
      throw new Error('CLI error');
    });
};
