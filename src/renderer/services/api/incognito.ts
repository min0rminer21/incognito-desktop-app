import { incognitoApiHttp } from '../http';
import { IPtokenRawApiResponse } from './types';

export const getPTokenSet = async () => {
  const rawData = await incognitoApiHttp.get<IPtokenRawApiResponse[]>(
    'ptoken/list'
  );

  if (rawData instanceof Array) {
    return rawData;
  }
  return [];
};
