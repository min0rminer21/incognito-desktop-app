export interface IPtokenRawApiResponse {
  ID: number;
  CreatedAt: string;
  UpdatedAt: string;
  TokenID: string;
  Symbol: string;
  OriginalSymbol: string;
  Name: string;
  ContractID: string;
  Decimals: number;
  PDecimals: number;
  Status: number;
  Type: number;
  CurrencyType: number;
  PSymbol: string;
  Default: boolean;
  UserID: number;
  PriceUsd: number;
  Verified: boolean;
  LiquidityReward: number;
  PercentChange1h: string;
  PercentChangePrv1h: string;
  CurrentPrvPool: number;
  PricePrv: number;
  volume24: number;
  ParentID: number;
  ListChildToken: string[];
}
