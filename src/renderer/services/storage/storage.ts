import { IncognitoStorageMethods } from '../../../common/types/incognitoStorageMethod';

const incognitoStorageExposed: IncognitoStorageMethods = (window as any)
  .electron.storage;

export const setItem = async (key: string, value: any) => {
  incognitoStorageExposed.set(key, value);
};

export const getItem = <T>(key: string) => {
  return incognitoStorageExposed.get<T>(key);
};

export const removeItem = (key: string) => {
  return incognitoStorageExposed.delete(key);
};
